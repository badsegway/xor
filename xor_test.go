package xor

import (
    "testing"
    "fmt"
)

func TestXor(t *testing.T) {
    key := "XYZ"
    data := "hardcoded secret"
    
    encrypted := EncryptDecrypt(data, key)
    fmt.Println("Encrypted:", encrypted)
    
    decrypted := EncryptDecrypt(encrypted, key)
    fmt.Println("Decrypted:", decrypted)
}
